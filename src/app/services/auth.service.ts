import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CookieService } from 'ngx-cookie-service';

import { map } from 'rxjs/operators';

import { IBody, ILoginResponse } from "./../interfaces/auth.interface";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  baseUrl: string;

  constructor(private http: HttpClient, private cookieService: CookieService) {
    this.baseUrl = "http://localhost:8080";
  }

  private buildUrl(route: string): string {
    return `${this.baseUrl}/${route}`;
  }

  public login(body: IBody) {
    const url = this.buildUrl("login");

    return this.http.post(url, body).pipe(
      map(
        (response: ILoginResponse) => {
          if (response.success && response.token) {
            this.cookieService.set('auth-token', response.token);
          }

          return true;
        }
      )
    );
  }

  public checkLogin(): boolean {
    const token = this.cookieService.get('auth-token');
    return !!token;
  }
}
