export interface IBody {
  email: string;
  password: string;
}

export interface ILoginResponse {
  success: boolean;
  token: string;
}