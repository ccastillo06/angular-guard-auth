import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";

import { AuthService } from "./../../services/auth.service";

@Component({
  selector: "app-loginform",
  templateUrl: "./loginform.component.html",
  styleUrls: ["./loginform.component.scss"]
})
export class LoginformComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      email: ["upgrade@hub.com"],
      password: ["1234abc"]
    });
  }

  ngOnInit() {}

  formSubmit(value) {
    this.authService.login(value).subscribe(
      response => {
        if (response) {
          this.router.navigate(["/protected"]);
        }
      },
      err => {
        console.log(err);
      }
    );
  }
}
