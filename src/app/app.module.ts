import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule  } from '@angular/forms'
// Import cookie service and inject as provider in our app
import { CookieService } from 'ngx-cookie-service';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ProtectedPageComponent } from "./components/protected-page/protected-page.component";
import { LoginformComponent } from './components/loginform/loginform.component';

@NgModule({
  declarations: [AppComponent, ProtectedPageComponent, LoginformComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule, HttpClientModule],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule {}
