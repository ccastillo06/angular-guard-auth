import { AuthGuard } from './guards/auth.guard';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ProtectedPageComponent } from "./components/protected-page/protected-page.component";
import { LoginformComponent } from "./components/loginform/loginform.component";

const routes: Routes = [
  {
    path: "",
    component: LoginformComponent
  },
  { path: "protected", component: ProtectedPageComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
